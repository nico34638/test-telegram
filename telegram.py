# importing all required libraries
from telethon.sync import TelegramClient
from telethon.tl.types import InputPeerUser, InputPeerChannel
from telethon import TelegramClient, sync, events
from pprint import pprint

from pattern_detector import PatternDetector

# get your api_id, api_hash, token
# from telegram as described above
api_id = '6319702'
api_hash = '02d5b1f83e47863e343ad6831d30ec77'
message = "Working..."

# your phone number
phone = '+330782860368'

# creating a telegram session and assigning
# it to a variable client
client = TelegramClient('session', api_id, api_hash)

# connecting and building the session
client.connect()

# in case of script ran first time it will
# ask either to input token or otp sent to
# number or sent or your telegram id
if not client.is_user_authorized():

    client.send_code_request(phone)

    # signing in the client
    client.sign_in(phone, input('Enter the code: '))

try:
    # receiver user_id and access_hash, use
    # my user_id and access_hash for reference
    dialogs = client.get_dialogs()
    pprint(dialogs)

    aChannel = client.get_entity('Noé')

    pattern_detector = PatternDetector("{trend} {symbol} {entry_price} TP : {tp} SL : {sl}")

    print("==================")
    for message in client.iter_messages('Noé'):
        print(pattern_detector.extract_pattern(message.message), '\n')

except Exception as e:

    # there may be many error coming in while like peer
    # error, wwrong access_hash, flood_error, etc
    print(e);

# disconnecting the telegram session
client.disconnect()
