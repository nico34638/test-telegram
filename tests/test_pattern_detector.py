import unittest
from pattern_detector import PatternDetector

class TestPatternDetector(unittest.TestCase):

    def test_if_size_equals(self):
        pattern_detector = PatternDetector("{trend} {symbol} {entry_price} TP : {tp} SL : {sl}")
        self.assertEqual(True, pattern_detector.check_if_size_is_equals("BUY EURUSD 1.276 TP : 1.678 SL : 1.8748"))

    def test_if_size_not_equals(self):
        pattern_detector = PatternDetector("{trend} {symbol} {entry_price} TP : {tp} SL : {sl}")
        self.assertEqual(False, pattern_detector.check_if_size_is_equals("BUY  1.276 1.678 SL : 1.8748"))

    def test_extract_pattern(self):
        pattern_detector = PatternDetector("{trend} {symbol} {entry_price} TP : {tp} SL : {sl}")
        self.assertEqual("BUY EURUSD 1.276 1.678 1.8748", pattern_detector.extract_pattern("BUY EURUSD 1.276 TP : 1.678 SL : 1.8748"))

    def test_extract_pattern_with_wrong_pattern(self):
        pattern_detector = PatternDetector("{trend} {symbol} {entry_price} TP : {tp} SL : {sl}")
        self.assertNotEqual("BUY EURUSD 1.276 1.678 1.8748", pattern_detector.extract_pattern("BUY EURUSD 1.276 TP :  SL  1.8748"))

if __name__ == '__main__':
    unittest.main()
