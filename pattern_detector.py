import re

class PatternDetector:

    def __init__(self, pattern):
        self.pattern = pattern
        self.pattern_split = self.pattern.split(" ")

    def check_if_size_is_equals(self, message):
        message = self.string_to_one_line(message)
        message_split = message.split(" ")
        res = False
        if len(self.pattern_split) == len(message_split):
            res = True
        return res

    def string_to_one_line(self, string):
        line = ""
        for character in string:
            if character != '\n':
                line += character
            else:
                line += " "
        return line

    def extract_pattern(self, message):
        message = self.string_to_one_line(message)
        if self.check_if_size_is_equals(message):
            message_split = message.split(" ")
            counter = 0
            res = ""
            for element in  self.pattern_split:
                result = re.match("\{.*}", element)
                if result is not None:
                    res += message_split[counter] + " "
                counter += 1
            res = res[:-1]
            return res
        else:
            return ""
